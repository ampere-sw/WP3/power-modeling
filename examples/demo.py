#!/usr/bin/env python3

# Copyright 2022 ETH Zurich and University of Bologna.
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
# http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Author: Sergio Mazzola <smazzola@iis.ee.ethz.ch>

import os
import power_model
from power_model.common.utils import load_json

DEFAULT_CONFIG_DIR = os.path.join(power_model.__path__[0], '../config')
DEFAULT_PLATFORM   = os.path.join(DEFAULT_CONFIG_DIR, 'platform.json')
DEFAULT_CPU_MODEL  = os.path.join(DEFAULT_CONFIG_DIR, 'cpu_model.json')
DEFAULT_GPU_MODEL  = os.path.join(DEFAULT_CONFIG_DIR, 'gpu_model.json')

model = power_model.PowerModel(
    platform_json='./config/platform.json',
    cpu_model_json='./config/cpu_model.json',
    gpu_model_json='./config/gpu_model.json'
)

## CPU ##

# Load dummy CPU PMCs samples into a dict
pmcs_cpu = load_json('./examples/cpu_sample.json', keys2int=True)

# Define conditions of acquired PMCs samples
freq_cpu = 729600000 # in Hz
sample_per_cpu = 0.50 # in s
# Estimate the avg power of core 4
core_i = 4
print('Core {}: {:.2f} mW'.format(core_i, model.power_cpu_core_i(core_i, pmcs_cpu[core_i], freq_cpu, sample_per_cpu)))
# Estimate the avg power of whole CPU
print('CPU: {:.2f} mW'.format(model.power_cpu(pmcs_cpu, freq_cpu, sample_per_cpu)))

## GPU ##

# Load dummy GPU PMCs samples into a dict
pmcs_gpu = load_json('./examples/gpu_sample.json', keys2int=True)

# Define conditions of acquired PMCs samples
freq_gpu = 624750000 # in Hz
sample_per_gpu = 0.10 # in s
# Estimate the avg power of GPU
print('GPU: {:.2f} mW'.format(model.power_gpu(pmcs_gpu, freq_gpu, sample_per_gpu)))

## Combined ##
sample_per = 2.1
print('SoC: {:.2f} mW'.format(model.power_soc(pmcs_cpu, freq_cpu, pmcs_gpu, freq_gpu, sample_per)))
