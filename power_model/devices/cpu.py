# Copyright 2022 ETH Zurich and University of Bologna.
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
# http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Author: Sergio Mazzola <smazzola@iis.ee.ethz.ch>

import pandas as pd

def pmcs_list(x: dict) -> int:
    """ Return the list of configurable PMCs (excluding the fixed clock cycle counter)

    @param x: Structure containing some hierarchy describing PMCs
    """
    return list(x['conf_pmcs'].keys())

def params_core_i(pmcs: dict, freq: int, sample_per: float, model_per: float) -> pd.DataFrame:
    """ Compute model parameters from raw PMCs input; this function's implementation depends on model's structure

    @param pmcs: PMC samples as {'clk':int, 'conf_pmcs':{pmc0:int, pmc1:int, pmc2:int}}
    @param freq: Clock frequency (in Hz) during profiling of the acquired dataset
    @param sample_per: Sampling period (in s) referred to the PMC values (i.e. interval of accumulation)
    @param model_per: Sampling period (in s) used to collect the PMC samples for model training
    """
    # Fill up the values for the model independent variables
    # CPU power model parameters structure (flattens the one of the config file):
    # activity, clk, pmc0, pmc1, pmc2, const
    params = {}
    # Scaling factor to normalize PMC values to the sample period of model training
    scaling = model_per/sample_per
    params['activity'] = (pmcs['clk'] / sample_per) / freq
    for pmc in pmcs_list(pmcs):
        params[pmc] = pmcs['conf_pmcs'][pmc] * scaling
    params['const'] = 1
    # Convert to pandas DataFrame
    return pd.DataFrame([params])

def weights_core_i(model:dict) -> pd.DataFrame:
    """ Organizes model weights in a flat data structure for model computation; this function's implementation depends on model's structure

    @param model: Weights of the trained model at this freq for this core {'activity':float, 'clk':float, 'conf_pmcs':{pmc0:float, pmc1:float, pmc2:float}, 'const':float}
    """
    weights = {}
    weights['activity'] = model['activity']
    for pmc in pmcs_list(model):
        weights[pmc] = model['conf_pmcs'][pmc]
    weights['const'] = model['const']
    # Convert to pandas DataFrame
    return pd.DataFrame([weights])

def estim_power_core_i(i: int, model: dict, pmcs: dict, freq: int, sample_per: float, model_per: float) -> float:
    """ Estimate the avg power consumption of core i at the given frequency over the given sampling time

    @param i: Index of the core whose power is being estimated
    @param model: Weights of the trained model at this freq for this core
    @param pmcs: PMC samples
    @param freq: Clock frequency (in Hz) during profiling of the acquired dataset
    @param sample_per: Sampling period (in s) referred to the PMC values (i.e. interval of accumulation)
    @param model_per: Sampling period (in s) used to collect the PMC samples for model training
    """
    # Process independent model parameters on which to run the linear model
    params = params_core_i(pmcs, freq, sample_per, model_per)
    weights = weights_core_i(model)
    # Validate inputs
    if not set(params.columns) == set(weights.columns):
        raise Exception('Model of core {} @ frequency {} Hz has different parameters than input sample.'.format(i, freq))
    # Run model
    weights = weights.transpose()
    power_estim = params.dot(weights)
    return power_estim.iat[0,0]
