# Copyright 2022 ETH Zurich and University of Bologna.
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
# http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Author: Sergio Mazzola <smazzola@iis.ee.ethz.ch>

import pandas as pd

def pmcs_max_inst(x: dict) -> int:
    """ Return the maximum number of instances of the considred GPU events

    @param x: Structure containing some hierarchy describing PMCs
    """
    return max(map(len, x.values()))

def pmcs_list(x: dict) -> int:
    """ Return the list of configurable PMCs

    @param x: Structure containing some hierarchy describing PMCs
    """
    return list(x['conf_pmcs'].keys())

def params_gpu(pmcs: dict, sample_per: float) -> pd.DataFrame:
    """ Compute model parameters from raw PMCs input; this function's implementation depends on model's structure

    @param pmcs: PMC samples as {pmc0:{inst0:int, isnt1:int, ...}, pmc1:{...}, ...}
    @param sample_per: Sampling period (in s) referred to the PMC values (i.e. interval of accumulation)
    """
    # Fill up the values for the model independent variables
    params_raw = pd.DataFrame.from_dict(pmcs)
    # Normalize with respect to time elapsed (= event rate)
    params_raw = params_raw/(sample_per * 1e9)
    # Average instances
    # events with 8 instances -> average all of them
    params_8i = params_raw.loc[:, params_raw.count(axis=0) == pmcs_max_inst(pmcs)].mean(axis=0, skipna=True)
    # events with 4 instances -> average instances 0 and 1
    params_4i = params_raw.loc[0:1, params_raw.count(axis=0) < pmcs_max_inst(pmcs)].mean(axis=0, skipna=True)
    # Concatenate PMC isntances averages and add constant term
    params = pd.DataFrame(pd.concat([params_8i, params_4i])).transpose()
    params['const'] = 1
    return params

def weights_gpu(model:dict) -> pd.DataFrame:
    """ Organizes model weights in a flat data structure for model computation; this function's implementation depends on model's structure

    @param model: Weights of the trained model at this freq {pmc1:float, pmc2:float, ..., 'const':float}
    """
    weights = {}
    for pmc in pmcs_list(model):
        weights[pmc] = model['conf_pmcs'][pmc]
    weights['const'] = model['const']
    return pd.DataFrame([weights])

def estim_power_gpu(model: dict, pmcs: dict, freq: int, sample_per: float) -> float:
    """ Estimate the avg power consumption of the GPU at the given frequency over the given sampling time

    @param model: Weights of the trained model at this freq
    @param pmcs: PMC samples
    @param freq: Clock frequency (in Hz) during profiling of the acquired dataset
    @param sample_per: Sampling period (in s) referred to the PMC values (i.e. interval of accumulation)
    """
    # Process independent model parameters on which to run the linear model
    # print all inputs
    params = params_gpu(pmcs, sample_per)
    weights = weights_gpu(model)
    # Validate inputs
    if not set(params.columns) == set(weights.columns):
        raise Exception('GPU model @ frequency {} Hz has different parameters than input sample.'.format(freq))
    # Run model
    weights = weights.transpose()
    power_estim = params.dot(weights)
    return power_estim.iat[0,0]