# Copyright 2022 ETH Zurich and University of Bologna.
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
# http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Author: Sergio Mazzola <smazzola@iis.ee.ethz.ch>

import json

def str2int_keys(item):
    """ Convert keys loaded from json dict from string to int

    @param item: Entry parsed from a json, used only if it is a dict
    """
    if isinstance(item, dict): #and all(key.isdigit() for key in item.keys())
            return {int(k) if k.isdigit() else k :v for k,v in item.items()}
    return item

def load_json(filename: str, keys2int: bool=False) -> dict:
    """ Load dict from a json file

    @param filename: Path of the json file to read in
    @param keys2int: If dict keys to int if they represent digits
    """
    with open(filename) as json_file:
        if keys2int:
            data = json.load(json_file, object_hook=str2int_keys)
        else:
            data = json.load(json_file)
    return data
