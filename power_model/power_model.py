# Copyright 2022 ETH Zurich and University of Bologna.
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
# http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Author: Sergio Mazzola <smazzola@iis.ee.ethz.ch>

import os
import power_model
from power_model.devices import cpu
from power_model.devices import gpu
from power_model.common import utils

DEFAULT_CONFIG_DIR = os.path.join(power_model.__path__[0], '../config')
DEFAULT_PLATFORM   = os.path.join(DEFAULT_CONFIG_DIR, 'platform.json')
DEFAULT_CPU_MODEL  = os.path.join(DEFAULT_CONFIG_DIR, 'cpu_model.json')
DEFAULT_GPU_MODEL  = os.path.join(DEFAULT_CONFIG_DIR, 'gpu_model.json')

class PowerModel():
    def __init__(self, platform_json: str=DEFAULT_PLATFORM, cpu_model_json: str=DEFAULT_CPU_MODEL, gpu_model_json: str=DEFAULT_GPU_MODEL) -> None:
        """ Constructor for PowerModel object

        @param platform_json: Json file describing platform parameters
        @param cpu_model_json: Json file containing the settings and a dictionary of param:weight for the CPU model
                               freq:core:{'activity':float, 'clk':float, 'conf_pmcs':{pmc0:float, pmc1:float, pmc2:float}, 'const':float}
        @param gpu_model_json: Json file containing the settings and a dictionary of param:weight for the GPU model
                               freq:{pmc1:float, pmc2:float, ..., 'const':float}
        """
        # Set platform parameters
        self.platform = utils.load_json(platform_json)
        # Set dict with CPU model (parameters: weights)
        self.cpu_model = utils.load_json(cpu_model_json, keys2int=True)
        # Set dict with GPU model (parameters: weights)
        self.gpu_model = utils.load_json(gpu_model_json, keys2int=True)

    def power_cpu_core_i(self, i: int, pmcs: dict, freq: int, sample_per: float) -> float:
        """ PMC-based estimation of the power consumption of core i (in mW)

        @param i: Index of the core whose power must be estimated
        @param pmcs: Dictionary of the PMCs samples to run the model on, {'clk':int, 'conf_pmcs':{pmc0:int, pmc1:int, pmc2:int}}
        @param freq: CPU clock frequency (in Hz) at which the PMC samples have been acquired
        @param sample_per: Sampling period (in s) referred to the PMC values (i.e. interval of accumulation)
        """
        return cpu.estim_power_core_i(i, self.cpu_model["model"][freq][i], pmcs, freq, sample_per, self.cpu_model["settings"]["sample_per"])

    def power_cpu(self, pmcs: dict, freq: int, sample_per: float) -> float:
        """ PMC-based estimation of the CPU power consumption (in mW)

        @param pmcs: Per-core dictionary of the PMCs samples to run the model on, {0:{'clk':int, 'conf_pmcs':{pmc0:int, pmc1:int, pmc2:int}}, ...}
        @param freq: CPU clock frequency (in Hz) at which the PMC samples have been acquired
        @param sample_per: Sampling period (in s) referred to the PMC values (i.e. interval of accumulation)
        """
        power_tot = 0
        for i in range(self.platform["num_cores_cpu"]):
            power_tot = power_tot + self.power_cpu_core_i(i, pmcs[i], freq, sample_per)
        return power_tot

    def power_gpu(self, pmcs: dict, freq: int, sample_per: float) -> float:
        """ PMC-based estimation of the GPU power consumption (in mW)

        @param pmcs: Per-PMC, per-event-instance dictionary of the PMCs samples to run the model {pmc0:{inst0:int, isnt1:int, ...}, pmc1:{...}, ...}
        @param freq: GPU clock frequency (in Hz) at which the PMC samples have been acquired
        @param sample_per: Sampling period (in s) referred to the PMC values (i.e. interval of accumulation)
        """
        return gpu.estim_power_gpu(self.gpu_model["model"][freq], pmcs, freq, sample_per)

    def power_soc(self, pmcs_cpu: dict, freq_cpu: int, pmcs_gpu: dict, freq_gpu: int, sample_per: float) -> float:
        """ PMC-based estimation combining CPU and GPU power estimations (in mW)

        @param pmcs_cpu: Per-core dict of the CPU PMCs samples to run the model, {0:{'clk':int, 'conf_pmcs':{pmc0:int, pmc1:int, pmc2:int}}, ...}
        @param freq_cpu: CPU clock frequency (in Hz) at which the PMC samples have been acquired
        @param pmcs_gpu: Per-PMC, per-event-instance dictionary of the GPU PMCs samples to run the model, {pmc0:{inst0:int, isnt1:int, ...}, pmc1:{...}, ...}
        @param freq_gpu: GPU clock frequency (in Hz) at which the PMC samples have been acquired
        @param sample_per: Sampling period (in s) referred to the PMC values (i.e. interval of accumulation)
        """
        power_cpu = self.power_cpu(pmcs_cpu, freq_cpu, sample_per)
        power_gpu = self.power_gpu(pmcs_gpu, freq_gpu, sample_per)
        return power_cpu + power_gpu

    def energy_cpu_core_i(self, i: int, pmcs: dict, freq: int, sample_per: float) -> float:
        """ Energy consumption (in mJ) estimation of core i based on PMC-based power model

        *Same parameters as power_cpu_core_i*
        """
        power = self.power_cpu_core_i(i, pmcs, freq, sample_per)
        return power * sample_per

    def energy_cpu(self, pmcs: dict, freq: int, sample_per: float) -> float:
        """ CPU energy (in mJ) consumption estimation based on PMC-based power model

        *Same parameters as power_cpu*
        """
        power = self.power_cpu(pmcs, freq, sample_per)
        return power * sample_per

    def energy_gpu(self, pmcs: dict, freq: int, sample_per: float) -> float:
        """ GPU energy (in mJ) consumption estimation based on PMC-based power model

        *Same parameters as power_gpu*
        """
        power = self.power_gpu(pmcs, freq, sample_per)
        return power * sample_per

    def energy_soc(self, pmcs_cpu: dict, freq_cpu: int, pmcs_gpu: dict, freq_gpu: int, sample_per: float) -> float:
        """ Energy consumption (in mJ) estimation of CPU and GPU combined, based on PMC-based power model

        *Same parameters as power_soc*
        """
        power = self.power_soc(pmcs_cpu, freq_cpu, pmcs_gpu, freq_gpu, sample_per)
        return power * sample_per
