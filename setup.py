# Copyright 2022 ETH Zurich and University of Bologna.
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
# http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Author: Sergio Mazzola <smazzola@iis.ee.ethz.ch>

#TODO: Move to pyproject.toml (mandatory from pip 23.1)

from setuptools import setup, find_packages

with open('requirements.txt') as f:
    requirements = f.readlines()

setup(
    name='power-modeling',
    version='0.1',
    author = 'Sergio Mazzola',
    author_email = 'smazzola@iis.ee.ethz.ch',
    license='Apache License, Version 2.0. Copyright 2022 ETH Zurich and University of Bologna.',
    description = 'A Python-based energy optimization and estimation framework',
    long_description=open('README.md').read(),
    long_description_content_type = 'text/markdown',
    url='https://gitlab.bsc.es/ampere-sw/WP3/power-modeling.git',
    packages=find_packages(),
    install_requires=requirements,
    python_requires = '>=3.8'
)
