# power-modeling
Power estimation tool based on performance monitoring counters (PMCs).
This tool is meant to estimate the average power consumption of a task over a given sampling period, during which the PMC values have been accumulated.

## Requirements
- Python >=3.8
- For the additionally required Python packages, see `requirements.txt`

## Installation
It is suggested to install `power-modeling` in a virtual environment. For example, for `venv`:
```bash
git clone https://gitlab.bsc.es/ampere-sw/WP3/power-modeling.git
cd power-modeling
python3 -m venv .venv
source .venv/bin/activate
python3 -m pip install .
```
Make sure that `python3` points to a Python version complying with the requirements in the previous section.

To exit the virtual environment and uninstall:
```bash
deactivate
rm -rf .venv build
```

Alternatively, you can also directly install `power-modeling` in your own environment.

## Usage & Example
Once installed, the package is accessible from within python with
```Python
import power_model
```

To run the power models and estimate the power consumption, the following main inputs are required from the user:
- platform configuration
- CPU power model (weights for all independent parameters, for each core, at all desired frequencies)
- GPU power model (weights for all independent parameters, at all desired frequencies)
- activity information whose power needs to be estimated
    - PMCs samples
    - clock frequency from the profiling of the PMCs
    - sample period, i.e. interval of time corresponding to the PMCs measurements

Refernce platform configuration and power models for the Jetson AGX Xavier's CPU (ARM-based 8-core Carmel SoC) and GPU (NVIDIA Volta GPU) are provided in the `config` directory.
Examples for the PMCs sample structures required by the tool are provided in the `examples` directory.
An example usage is provided in `examples/demo.py`. You can launch it with:
```bash
python3 examples/demo.py
```
